\documentclass[10pt]{letter}
\usepackage{UPS_letterhead,xcolor,mhchem,ragged2e,hyperref}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\definecolor{darkgreen}{HTML}{009900}


\begin{document}

\begin{letter}%
{To the Editors of the Journal of Chemical Physics,}

\opening{Dear Editors,}

\justifying
Please find attached a revised version of the manuscript entitled 
\begin{quote}
	\textit{``Benchmarking CASPT3 Vertical Excitation Energies ''}.
\end{quote}
We thank the reviewers for their constructive comments and to support publication of the present manuscript.
Our detailed responses to their comments can be found below.
For convenience, changes are highlighted in red in the revised version of the manuscript. 

We look forward to hearing from you.

\closing{Sincerely, the authors.}

\newpage

%%% REVIEWER 1 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#1}
 
{Finally, some benchmarking on the CASPT3 method. 
This should have been done long ago -- the authors are kicking in an open door. 
A few things need to be modified -- minor -- for the paper to be accepted.  
}
\\
\alert{
We thank the reviewer for these positive comments and supporting publication of the present manuscript.
}

\begin{enumerate}

\item 
{Table I should be moved to the SI. 
Key entries could be kept if explicitly discussed in the manuscript.}
\\
\alert{
We would prefer to keep Table I in the main text as it is the core table of the present manuscript.
We believe that it would be unfortunate to move (parts of) the table in the SI as it would not be ideal for the reader to have to switch between the main article and the SI depending on the system he/she is interested in.
We hope that the reviewer will understand our motivations.
}

\item 
{It should be stressed that the conclusions are only valid for medium-sized organic molecules. 
The very important classes of transition metal complexes are not covered.}
\\
\alert{
We have modified the conclusion to stress these two points.
}

\item 
{Make it VERY clear that the excitation energies are Single-State CASPTX excitation energies.}
\\
\alert{
The reviewer is right. 
We have added this additional information in the "Computational Details" section of the manuscript.
}

\item 
{The authors should point out the difference between dynamic sigma polarization and so-called left-right polarization (https://doi.org/10.1021/jp9528020).}
\\
\alert{
This has been clarified in the discussion section of our revised manuscript.
}

\end{enumerate}

%%% REVIEWER 2 %%%
\noindent \textbf{\large Authors' answer to Reviewer \#2}
 
{This is an interesting paper that shows a benchmark of CASPT3 energies using small organic molecules. 
Such studies are important and suitable for JCP, so in principle I am in favor of it. 
I have however some comments that I would like the authors consider. 
}
\\
\alert{
Thank you for recommending publication of the present manuscript.
Our response to Reviewer \#2's comments are given below.
}

\begin{enumerate}

\item 
{One straightforward request is that I would have liked to see also a histogram in fig 2 of the CASSCF values of Table 1. 
CASSCF has largest errors, sure, but it would be interesting to see how broad is the distribution in comparison to caspt3, represented in the same way.}
\\
\alert{
The histogram for CASSCF has been added (and discussed) in the revised version of the manuscript.
}

\item 
{My other, not so straightforward, comment refers to the IPEA correction. 
It is a valuable and welcome conclusion that the authors found that CASTP3 is less sensitive to IPEA. 
Still, in saying that CASPT3 is like CASPT2(NOIPEA) the paper seems to advocate that one should use IPEA in CASPT2. 
The first thing that authors should do is to point out that the IPEA shift is differently implemented in MOLPRO (used in this paper) than in MOLCAS (used in ref 56 which recommends not to use IPEA for the same type of organic molecules) and therefore, this apparent conclusion should be taken with caution. 
Second, this conclusion relies on the chosen theoretical best estimates (TBE). 
The TBEs (for molecules of the Thiel set, \url{https://lcpq.github.io/QUESTDB_website/subsets/}, QUEST3 subset) seems to be largely CC3, CCSDT, CCSDTQ, and NEVPT2 with aug-TZ/QZ basis sets. 
From a theoretical point of view, this should give an impressive accuracy, as they are high-order wave function based methods and large basis sets. 
And indeed, CASPT2/IPEA performs similar to CCSD/CC3/NEVPT2 excitation energies for the Thiel set. 
However, all these methods (CCSD/CC3/NEVPT2/IPEA) using the TZVP basis set overestimate experimental excitation energies, while NOIPEA underestimates excitation energies of the Thiel set, and this underestimation with the TZVP basis set was smaller in magnitude than the overestimation of the other methods, making NOIPEA with TZVP basis set preferable. 
The question is, does one want to be as good as CASPT2 or as good as the experimental value? 
I would say the latter, so while the TBEs are useful, a comparison with the experimental values should be provided and the results also discussed in this light.}
\\
\alert{
Concerning the first point, we have added, in the "Computational Details" section, a comment stressing the fact that the implementation of the IPEA shift in MOLPRO and MOLCAS are not exact identical (yet very similar): \textit{``Note that the implementation of the IPEA shift is not exactly identical in MOLPRO (used here) and in MOLCAS (used, for example, in Ref. 56), since in MOLPRO the singly external configurations are not  contracted in the RS2 scheme.''}\\
Concerning the second point, comparison with experimental values are much trickier as it involves computing 0-0 energies which are the average of the vertical excitation energies from the ground- and excited-state equilibrium geometries corrected for zero-point vibrational energies (ZPVEs).
As shown in our earlier works [see ChemPhotoChem 3, 684 (2019) for a recent review], these are much more computationally expensive and depends strongly on the quality of the (absorption and emission) vertical energies.
The two other effects (ground- and excited-state geometries and ZPVEs) are less important.
Therefore, it is clear that vertical excitation energies are the key quantities to reproduce.
We have added a comment on this point at the end of the "Introdcution" section.
}

\item 
{Can the authors also discuss if there is any basis set effect? 
Are the TBEs obtained with the same basis set as their calculations? 
Probably not. 
The authors could be more a bit more specific about the method/basis set of each of the TBEs in the SI, so that everyone can compare for a specific molecule (if desired) the setups in which the values where obtained. 
Again, this should be discussed against the experimental value in order to convey a clear recommendation to the readers.}
\\
\alert{
All calculations reported in the present manuscript have been performed with the very same basis as the TBEs (aug-cc-pVTZ), as mentioned in the "Computational Details" section. 
We have modified the corresponding sentence to stress this further.
Having an augmented triple-$\zeta$ basis allows us to describe faithfully all excited states (including the Rydberg states).
Therefore, there is no basis set effect between the CASPT2/CASPT3 and TBE excitation energies.
Moreover, because the basis set effects are extremely transferable within wave function methods, we can safely assume that the present conclusions would be identical in a smaller or larger basis set.
}

\end{enumerate}

\end{letter}
\end{document}
